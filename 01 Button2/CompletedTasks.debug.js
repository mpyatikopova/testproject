TFS.module("CompletedTasks",
    [
        "TFS.WorkItemTracking.Controls",
        "TFS.WorkItemTracking",
        "VSS\\Utils\\Core",
        "VSS\\Utils\\UI"
    ],
    //инициализация плагина
    function () {
        // сокращения namespace'ов
        var WITOM = TFS.WorkItemTracking,
            WITCONTROLS = TFS.WorkItemTracking.Controls,
            delegate = VSS.Core.delegate;

      
      
        //конструктор контрола
        function CompletedTasks(container, options, workItemType) {
            this.baseConstructor.call(this, container, options, workItemType);
        }
///

        //наследуемся от WorkItemControl (переопределяем методы)
        CompletedTasks.inherit(WITCONTROLS.WorkItemControl, {
            //разметка для контрола
            _control: null,
			_init: function () {
                this._base();
            },

            markup: '<button id="completed_tasks" style="height:30px; margin:1px; width: 180px;" align="left"> Несогласованные задачи </button>',
			
			 _windowResult:function(output) 
			{
			var ControlElements = require("VSS/Controls/Dialogs");
			var that=this;
			var dialog1 = ControlElements.ModalDialog.show(ControlElements.ModalDialog, 
			{
				title: "Несогласованные задачи",
				content: $(output),
				buttons: {
                            "ОК": function () {
                             dialog1.close();
							 }
						}
				});
			
			},
			_getChildLinks: function() {
				var that=this;
                return this._workItem.getLinks().filter(function(x) {
                    return x.baseLinkType == "WorkItemLink" && x.linkData.LinkType ==
                        that._workItem.store.getLinkTypeEnds().filter(function(x) { return x.immutableName == "System.LinkTypes.Hierarchy-Forward" })[0].id; 
                });
				
            },
			
			_links: function() 
			{
                if (!this._workItem)
                    return;
				
				var that=this;
				var s=0,j=0;
                var that = this;
				var id;
				var links = this._getChildLinks();
				if (links.length == 0) {
                    alert("No links");
					return;
                    } 
                var ControlElements = require("VSS/Controls/Dialogs");
				var ids =[];
				var names = ["Logrocon.ApproveDevelopment","Logrocon.ApproveTesting","Logrocon.Security" ];
				var labels = ["Согласование Разработки","Согласование Тестирования","Согласование ИБ"];
				var output = '<div><table><tr><td>ID</td><td>Поле</td><td>Значение</td></tr>';
				for (var i = 0; i < links.length; ++i) 
				{
					ids[i] = links[i].linkData.ID;
				}
				 
				   if (ids.length > 0) 
					   {
							this._workItem.store.beginGetWorkItems(ids,function(WorkItems) 
							{
							
								for (wi in WorkItems) 
								{
									for (var i = 0; i < names.length; ++i) 
									{
										var field = WorkItems[wi].getField(names[i]).getValue();
											if (( field != "Да" && i < 2) || (field != "согласовано" && field != "согласовано с условием" && i == 2))
											{ 
												id=WorkItems[wi].getField("System.Id").getValue();
												output += '<tr><td>'+id+'</td><td>'+labels[i]+'</td><td>'+field+'</td></tr>';
												++s;
											} 
									}
											i=0;
								}
									
								if(s>0)
								{
									output +='</table></div>';
									that._windowResult(output);
								}
								else if(s==0)
								{
									alert("Все Tasks выполнены");
								}
						   });
						}
								   
			},     
            

			_check: function()
			{
				var that = this;
				if (that._workItem.getField("Logrocon.AllApproved").getValue()==1) 
							{
								$("[id='completed_tasks']").attr("disabled", true);
								$("[id='completed_tasks']").html('Все согласовано');
							} else 
							{
								
								$("[id='completed_tasks']").html('Несогласованные задачи');
								$("[id='completed_tasks']").removeAttr("disabled");
							//that._initControl();
							}
				
			},

			 bind: function (workItem) 
			{
                var tfsContext = require("Presentation/Scripts/TFS/TFS.Host.TfsContext").TfsContext.getDefault();
                var that = this;
				
                that._workItem = workItem;
						var currentUser = tfsContext.currentIdentity;
					
						that._workItem = workItem;

						
						if (!that._control){
						
						that._control = $(that.markup).appendTo(that._container);
						
						
						var ctrl = $(that._control);
						var addButtons = $("[id='completed_tasks']");
						i++;
						
						
					that._check();
						for (var i = 0; i < addButtons.length; ++i) 
						{
							$(addButtons[i]).off();
							$(addButtons[i]).click(function () 
							{	
							if (that._workItem.getField("Logrocon.AllApproved").getValue()==1)  
							{
								$("[id='completed_tasks']").attr("disabled", true);
								$("[id='completed_tasks']").html('Все согласовано');
							} else 
							{
								$("[id='completed_tasks']").removeAttr("disabled");
								$("[id='completed_tasks']").html('Несогласованные задачи');
								that._links();
							}								
							
						});
						}
						}
				workItem.attachWorkItemChanged(function (sender, args) 
				{
                  _check();
				});
				//initControl();
			},

			///
			///
			
            unbind: function (workItem) 
			{
               this.base.unbind(workItem);
            },


            clear: function () 
			{
              if (this._control)
                   this._control.empty();
            },
       
});
        WITCONTROLS.registerWorkItemControl("CompletedTasks", CompletedTasks);

        return {
            CompletedTasks: CompletedTasks
        };
    });