TFS.module("Web.Logrocon_ReleaseBinder",

    [

        "TFS.WorkItemTracking.Controls",
        "TFS.WorkItemTracking",
        "VSS\\Utils\\Core",
        "VSS\\Utils\\UI"

    ],

    function() {

        var WITCONTROLS = TFS.WorkItemTracking.Controls,
            delegate = VSS.Core.delegate;

 

        function Logrocon_ReleaseBinder(container, options, workItemType) {
            this.baseConstructor.call(this, container, options, workItemType);
        }

 

        Logrocon_ReleaseBinder.inherit(WITCONTROLS.WorkItemControl, {
            _control: null,
            _workitem: null,

            _init: function() {
                var that = this;

                this._base();
                this._control = $('<div id="release_binder"></div>').appendTo(this._container);
 

                var Controls = require("VSS/Controls");
                var Combos = require("VSS/Controls/Combos");
                var options = { mode: "drop", allowEdit: false, value: "" };
                this._control = Controls.create(Combos.Combo, this._container, options);
            },

 

            _getReleaseID: function() {
                var str = this._getField().getValue();
                return parseInt(str.substring(str.indexOf("(") + 1, str.length - 1));
            },

 

            _getCustomLinks: function() {
                var that = this;
                return this._workItem.getLinks().filter(function(x) {
                    return x.baseLinkType == "WorkItemLink" && x.linkData.LinkType ==
                        that._workItem.store.getLinkTypeEnds().filter(function(x) { return x.immutableName == "Logrocon.LinkTypes.ReleaseHierarchy-Reverse" })[0].id;
                });
            },

 

            _link: function() {
                var that = this;
                if (!this._workItem)
                    return;
                var id = this._getReleaseID();
                var links = this._getCustomLinks();

                if (links.length == 0) {
                    if (!isNaN(id)) {
                        var link = TFS.WorkItemTracking.WorkItemLink.create(this._workItem, "Logrocon.LinkTypes.ReleaseHierarchy-Reverse", id, "");
                        this._workItem.addLink(link);
                        this._workItem.beginSave();
                    }
                } else {
                    var anc = links[0];
                    if (anc.linkData.ID != id) {
                        anc.remove();
                        this._workItem.beginSave();
                    }
                }
            },

 

            _redraw: function(flushing) {
                var that = this;
                if (!this._workItem)
                    return;

                require(["TFS/Core/RestClient"], function(RestClient) {
                    var client = RestClient.getClient();
                    var tfsContext = require("Presentation/Scripts/TFS/TFS.Host.TfsContext").TfsContext.getDefault();
                    client.getTeams(tfsContext.contextData.project.id).then(
                        function(teams) {
                            var team = teams.filter(function(x) { return x.name == "Release Team"; })[0];

                            client.getTeamMembers(tfsContext.contextData.project.id, team.id).then(

                                function(teamMembers) {

                                    var setValuesByCommandAndCurrentState = function(inReleaseCommand) {
                                        var links = that._getCustomLinks();
                                        var query = null
                                        var releaseMapped = links.length != 0;
                                        var releaseState = null;
                                        var setText = null;
                                        var list = [];
                                        var releaseId = null;

                                        var lastActions = function(inReleaseCommand, releaseMapped, query, releaseId, releaseState, setText, list) {
                                            if (inReleaseCommand) {
                                                query = "Select [System.Id], [System.Title] From WorkItems Where [System.WorkItemType] = 'Release'" +
                                                    " and [System.TeamProject] = @project and [System.State] <> 'Closed' and [System.State] in ('New', 'Planning', 'Approvement')";
                                                if (releaseMapped) {
                                                    query = query + " and [System.Id] <> " + releaseId;
                                                }
                                            }
                                            else //ты не в команде :(
                                            {
                                                if ((!releaseMapped) || releaseState == "New" || releaseState == "Planning") {
                                                    query = "Select [System.Id], [System.Title] From WorkItems Where [System.WorkItemType] = 'Release'" +
                                                        " and [System.TeamProject] = @project and [System.State] <> 'Closed' and [System.State] in ('New', 'Planning')";
                                                    if (releaseMapped) {
                                                        query = query + " and [System.Id] <> " + releaseId;
                                                    }
                                                }
                                            }

											if(query != null)
											{
												that._workItem.store.beginQuery(that._workItem.project, query, function(res) 
												{
													var items = res.payload.rows.map(function(x) { return x[3] + " (" + x[0] + ")"; });
													for (var i = 0; i < items.length; ++i) 
													{
														list.push(items[i]);
													}
													if (list.indexOf("Не в релизе") == -1) 
													{
														list.push("Не в релизе");
													}
													that._control.setInputText(setText);
													that._control.setSource(list);
												}, null, null);
											}
											else
											{
												that._control.setInputText(setText);
                                                that._control.setSource(list);
											}
                                            //в результате
                                        };

                                        if (releaseMapped) 
										{
                                            that._workItem.store.beginGetWorkItem(links[0].linkData.ID, function(release) 
											{
                                                releaseState = release.getState();
                                                var releaseText = release.getField("System.Title").getValue() + " (" + release.id + ")";
                                                setText = releaseText;
                                                releaseId = release.id
                                                list.push(releaseText);
                                                lastActions(inReleaseCommand, releaseMapped, query, releaseId, releaseState, setText, list);
                                            });
                                        }
                                        else 
										{
                                            setText = "Не в релизе";
                                            list.push("Не в релизе");
                                            lastActions(inReleaseCommand, releaseMapped, query, releaseId, releaseState, setText, list);
                                        }
                                    };

                                    setValuesByCommandAndCurrentState((teamMembers.filter(function(x) { return x.id == tfsContext.currentIdentity.id }).length > 0));

                                }
                            );
                        }
                    );
                });
            },


            // _setValueOfCurrentLink: function(setSource) {
            //    var links = this._getCustomLinks();
            //    var that = this;
            //    if (links.length == 0) {
            //        that._control.setInputText("Не в релизе");
            //        if (setSource) {
            //            that._control.setSource(["Не в релизе"]);
            //        		}
            //    } else {
            //            this._workItem.store.beginGetWorkItem(links[0].linkData.ID, function (wi) {
            //            that._getField().setValue(wi.getField("System.Title").getValue() + " (" + wi.id + ")");
            //            that._control.setInputText(that._getField().getValue());
            //            if (setSource) {
            //               that._control.setSource([that._getField().getValue()]);
            //            }
            //    });
            //   }
            // },

            _checkLinks: function(needsRefresh) 
			{
                if (!this._workItem)
                    return;

                var that = this;
                var ControlElements = require("VSS/Controls/Dialogs");
                var id = this._getReleaseID();
                var links = this._getCustomLinks();

               if (links.length > 0) 
			   {
                    this._workItem.store.beginGetWorkItem(links[0].linkData.ID, function(wi) {
                        that._getField().setValue(wi.getField("System.Title").getValue() + " (" + wi.id + ")");
                        if (needsRefresh)
                            that._workItem.beginSave(function() { that._workItem.beginRefresh(); });
                    });
                } else 
				{
                    that._getField().setValue("Не в релизе");
                    if (needsRefresh)
                        that._workItem.beginSave(function() { that._workItem.beginRefresh(); });
                }
            },

 

            bind: function(workitem) 
			{
                var that = this;
                this._workitem = workitem;
                this._base(workitem);

                this._control._bind("change", delegate(this, this._onChange));
                this._checkLinks(false);
                this._redraw(false);

                workitem.attachWorkItemChanged(function(sender, args) 
				{
                    if (args.change == "save-completed" || args.change == "refresh" || args.change == "reset") 
					{
                        if (args.change === "save-completed") 
						{
                            that._link();
                        } else 
						{
                            that._redraw(false);
                        }
                    }
                });
            },

 

            unbind: function(workitem) 
			{
                if (this._control) 
				{
                    this._control._unbind("change", delegate(this, this._onChange));
                    this._base(workitem);
                }
            },

 

            invalidate: function(flushing) 
			{
                //this._redraw(flushing);
            },

 

            clear: function() {
                if (this._control) 
				{
                    this._control.setInputText("Не в релизе");
                }
            },

 

            _onChange: function() 
			{
                if (this._control) 
				{
                    this._getField().setValue(this._control.getValue());
                }
            }
        });

 

        // Register this module as a work item custom control called "Logrocon_ReleaseBinder"
        WITCONTROLS.registerWorkItemControl("Logrocon_ReleaseBinder", Logrocon_ReleaseBinder);
        return 
		{
            Logrocon_ReleaseBinder: Logrocon_ReleaseBinder
        };
    });